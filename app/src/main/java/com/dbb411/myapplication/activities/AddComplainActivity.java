package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.ComplainData;
import com.dbb411.myapplication.models.UserData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddComplainActivity extends AppCompatActivity {

    public EditText complainTitle;
    public EditText complainDescription;
    public EditText complain_To;
    public Button register_button;
    public FirebaseAuth firebaseAuth;
    public FirebaseDatabase firebaseDatabase;
    public DatabaseReference databaseReference;
    public static int count = 0;
    UserData userData;


    void bindData(){
        complainTitle = (EditText) findViewById(R.id.complain_title);
        complainDescription = (EditText) findViewById(R.id.complain_description);
        register_button = (Button) findViewById(R.id.complain_register);
        complain_To = (EditText) findViewById(R.id.complain_to);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_complain);
        getSupportActionBar().setTitle("Add Complain");
        bindData();
        count++;

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        getComplainerName();
        register_button.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                if(checkValidation()){
                    String title = complainTitle.getText().toString();
                    String description = complainDescription.getText().toString();
                    String complainTo = complain_To.getText().toString();
                    
                    String complainBy = userData.getUserName();

                    databaseReference = firebaseDatabase.getReference("Complains");


                    ComplainData complainData = new ComplainData(title,description,complainBy ,complainTo,firebaseAuth.getUid());
                    String key = databaseReference.push().getKey();
                    assert key != null;
                    databaseReference.child(key).setValue(complainData).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(AddComplainActivity.this, "Complain added", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                    startActivity(new Intent(AddComplainActivity.this,HomeActivity.class));


                }
            }
        });
    }

    public Boolean checkValidation(){
        String title = complainTitle.getText().toString();
        String description = complainDescription.getText().toString();
        if(userData == null){
            Toast.makeText(this, "userData not recived", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(title.isEmpty() || description.isEmpty()){
            Toast.makeText(this, "Enter all data", Toast.LENGTH_SHORT).show();
            return false;
        }
        else{

            return true;
        }

    }

    public  void getComplainerName(){
        //FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        DatabaseReference databaseReference = firebaseDatabase.getReference("Users/"+firebaseAuth.getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userData = dataSnapshot.getValue(UserData.class);

                //Toast.makeText(AddComplainActivity.this, "complainBy"+ userData.getUserName(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(AddComplainActivity.this, "something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
