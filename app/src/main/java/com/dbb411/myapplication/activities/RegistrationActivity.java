package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.renderscript.Script;
import android.support.annotation.NonNull;
import android.support.v4.app.SupportActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.UserData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.security.PrivateKey;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName, userPassword, userEmail;
    private Button signUp;
    private TextView signIn;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().setTitle("Registration");

        bindingRegistrationData();

        firebaseAuth = FirebaseAuth.getInstance();

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidate()){
                    String user_email = userEmail.getText().toString().trim();
                    String user_password = userPassword.getText().toString().trim();

                    firebaseAuth.createUserWithEmailAndPassword(user_email,user_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                               sendEmailVerification();
                            }
                            else{
                                Toast.makeText(RegistrationActivity.this,"Registration fail",Toast.LENGTH_LONG);
                            }
                        }

                    });
                }
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegistrationActivity.this,LogInActivity.class));
            }
        });
    }

    private void bindingRegistrationData(){
        userName = (EditText) findViewById(R.id.userName);
        userEmail = (EditText) findViewById(R.id.userEmail);
        userPassword = (EditText) findViewById(R.id.userPassword);
        signUp = (Button) findViewById(R.id.signUp);
        signIn = (TextView) findViewById(R.id.signIn);
    }

    String uName,pwd,email;
    Boolean checkValidate(){
        uName = userName.getText().toString();
        pwd = userPassword.getText().toString();
        email = userEmail.getText().toString();

        if(uName.isEmpty() || pwd.isEmpty() || email.isEmpty()){
            Toast.makeText(this,"Enter all Data!!",Toast.LENGTH_LONG).show();

            return false;
        }
        else
            return true;
    }

    public void sendEmailVerification(){
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if(firebaseUser!=null){
            firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        sendUserData();
                        Toast.makeText(RegistrationActivity.this, "Successfully Registered, Verification mail sent",Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }

    public void sendUserData(){
        FirebaseDatabase firebaseDatabase =FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("Users");
        UserData userData = new UserData(uName,email);
        databaseReference.child(firebaseAuth.getUid()).setValue(userData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(RegistrationActivity.this, "finaly Data sent", Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(RegistrationActivity.this, "ohooooooo", Toast.LENGTH_SHORT).show();
                    task.getException().printStackTrace();
                }
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(RegistrationActivity.this,LogInActivity.class));
            }
        });

    }

}
