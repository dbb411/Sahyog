package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.adapters.DirectoryRecyclerAdapter;
import com.dbb411.myapplication.models.DirectoryItem;

public class DirectoryActivity extends AppCompatActivity {

    /** Android Views **/
    RelativeLayout rlJumpFeed;
    TextView tvFeed;
    RelativeLayout rlJumpNotification;
    TextView tvNotification;
    RelativeLayout rlJumpDirectory;
    TextView tvDirectory;
    RelativeLayout rlJumpAccount;
    TextView tvAccount;
    RecyclerView rvDirectory;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        rlJumpFeed = (RelativeLayout) findViewById(R.id.rl_jump_feed);
        tvFeed = (TextView) findViewById(R.id.tv_feed);
        rlJumpNotification = (RelativeLayout) findViewById(R.id.rl_jump_notification);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        rlJumpDirectory = (RelativeLayout) findViewById(R.id.rl_jump_directory);
        tvDirectory = (TextView) findViewById(R.id.tv_directory);
        rlJumpAccount = (RelativeLayout) findViewById(R.id.rl_jump_account);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        rvDirectory = (RecyclerView) findViewById(R.id.rv_directory);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);
        bindViews();
        getSupportActionBar().setTitle("Directory");
        setAllClickListeners();
        loadDataInRecyclerView();
    }

    private void loadDataInRecyclerView() {
        DirectoryRecyclerAdapter recyclerAdapter;
        recyclerAdapter = new DirectoryRecyclerAdapter(this, DirectoryItem.getDummyDirectoryItemList());
        rvDirectory.setLayoutManager(new LinearLayoutManager(this));
        rvDirectory.setAdapter(recyclerAdapter);
    }

    /**
     * This will assign click listeners to all the quick jump buttons.(Relative layouts)
     */
    private void setAllClickListeners() {
        rlJumpFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DirectoryActivity.this,HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                DirectoryActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });



        //click for Notification
        rlJumpNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DirectoryActivity.this,NotificationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                DirectoryActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for DirectoryActivity
        //avoid this as we are already on DirectoryActivity screen

        //click for account
        rlJumpAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DirectoryActivity.this,AccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                DirectoryActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
    }
}
