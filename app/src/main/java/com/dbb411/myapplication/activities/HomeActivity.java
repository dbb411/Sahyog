package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.adapters.FeedRecyclerAdapter;
import com.dbb411.myapplication.models.ComplainData;
import com.dbb411.myapplication.models.FeedItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    /** Android Views **/
    RelativeLayout rlJumpFeed;
    TextView tvFeed;
    RelativeLayout rlJumpNotification;
    TextView tvNotification;
    RelativeLayout rlJumpDirectory;
    TextView tvDirectory;
    RelativeLayout rlJumpAccount;
    TextView tvAccount;
    RecyclerView rvFeed;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        rlJumpFeed = (RelativeLayout) findViewById(R.id.rl_jump_feed);
        tvFeed = (TextView) findViewById(R.id.tv_feed);
        rlJumpNotification = (RelativeLayout) findViewById(R.id.rl_jump_notification);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        rlJumpDirectory = (RelativeLayout) findViewById(R.id.rl_jump_directory);
        tvDirectory = (TextView) findViewById(R.id.tv_directory);
        rlJumpAccount = (RelativeLayout) findViewById(R.id.rl_jump_account);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        rvFeed =(RecyclerView) findViewById(R.id.rv_feed);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindViews();
        getSupportActionBar().setTitle("Feed");
        setAllClickListeners();
        loadDataInRecyclerView();
    }

    /**
     * this will initialize the recycler adapter with dummy data and that adapter will be attached to the recyclerview
     */
    private void loadDataInRecyclerView() {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Complains").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(HomeActivity.this, "Data received", Toast.LENGTH_SHORT).show();
                List<FeedItem> feedItemList = new ArrayList<>();

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    ComplainData complain = postSnapshot.getValue(ComplainData.class);
                    Log.d("Get Data", complain.complainTitle);
                    feedItemList.add(new FeedItem(complain.complainTitle,complain.complainDescription,complain.complainBy,complain.complainTo));
                }
                FeedRecyclerAdapter recyclerAdapter;
                recyclerAdapter = new FeedRecyclerAdapter(HomeActivity.this,feedItemList);
                rvFeed.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
                rvFeed.setAdapter(recyclerAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(HomeActivity.this, "Data failed", Toast.LENGTH_SHORT).show();
                databaseError.toException().printStackTrace();
            }
        });

        //FeedRecyclerAdapter recyclerAdapter;
        //recyclerAdapter = new FeedRecyclerAdapter(this, FeedItem.getDummyFeedItemList());
        //rvFeed.setLayoutManager(new LinearLayoutManager(this));
        //rvFeed.setAdapter(recyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_search:
                Toast.makeText(this, "Search feature is coming soon.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_add:
                startActivity(new Intent(HomeActivity.this,AddComplainActivity.class));
                break;
        }
        return true;
    }

    /**
     * This will assign click listeners to all the quick jump buttons.(Relative layouts)
     */
    private void setAllClickListeners() {
        //avoid feed click as we are already on the page

        //click for notification
        rlJumpNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,NotificationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                HomeActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for directory
        rlJumpDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,DirectoryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                HomeActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for account
        rlJumpAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,AccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                HomeActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
    }
}
