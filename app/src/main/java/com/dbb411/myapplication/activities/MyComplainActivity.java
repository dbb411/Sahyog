package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.adapters.FeedRecyclerAdapter;
import com.dbb411.myapplication.models.ComplainData;
import com.dbb411.myapplication.models.FeedItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MyComplainActivity extends AppCompatActivity {

    /** Android Views **/
    RelativeLayout rlJumpFeed;
    TextView tvFeed;
    RelativeLayout rlJumpNotification;
    TextView tvNotification;
    RelativeLayout rlJumpDirectory;
    TextView tvDirectory;
    RelativeLayout rlJumpAccount;
    TextView tvAccount;
    RecyclerView rvcomplain;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        rlJumpFeed = (RelativeLayout) findViewById(R.id.rl_jump_feed);
        tvFeed = (TextView) findViewById(R.id.tv_feed);
        rlJumpNotification = (RelativeLayout) findViewById(R.id.rl_jump_notification);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        rlJumpDirectory = (RelativeLayout) findViewById(R.id.rl_jump_directory);
        tvDirectory = (TextView) findViewById(R.id.tv_directory);
        rlJumpAccount = (RelativeLayout) findViewById(R.id.rl_jump_account);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        rvcomplain =(RecyclerView) findViewById(R.id.rv_myComplain);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_complain);
        bindViews();
        getSupportActionBar().setTitle("My Complains");
        loadDataInRecyclerView();
    }

    /**
     * this will initialize the recycler adapter with dummy data and that adapter will be attached to the recyclerview
     */
    private void loadDataInRecyclerView() {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Complains").orderByChild("complainerId").equalTo(firebaseAuth.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Toast.makeText(MyComplainActivity.this, "Data received", Toast.LENGTH_SHORT).show();
                List<FeedItem> feedItemList = new ArrayList<>();

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    ComplainData complain = postSnapshot.getValue(ComplainData.class);
                    Log.d("Get Data", complain.complainTitle);
                    feedItemList.add(new FeedItem(complain.complainTitle,complain.complainDescription,complain.complainBy,complain.complainTo));
                }
                FeedRecyclerAdapter recyclerAdapter;
                recyclerAdapter = new FeedRecyclerAdapter(MyComplainActivity.this,feedItemList);
                rvcomplain.setLayoutManager(new LinearLayoutManager(MyComplainActivity.this));
                rvcomplain.setAdapter(recyclerAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MyComplainActivity.this, "Data failed", Toast.LENGTH_SHORT).show();
                databaseError.toException().printStackTrace();
            }
        });

        //FeedRecyclerAdapter recyclerAdapter;
        //recyclerAdapter = new FeedRecyclerAdapter(this, FeedItem.getDummyFeedItemList());
        //rvFeed.setLayoutManager(new LinearLayoutManager(this));
        //rvFeed.setAdapter(recyclerAdapter);
    }

}
