package com.dbb411.myapplication.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogInActivity extends AppCompatActivity {

    public EditText loginUserEmail,loginUserPassword;
    public Button signIn;
    public TextView signUp;
    public FirebaseAuth firebaseAuth;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        getSupportActionBar().setTitle("Login");
        bindingloginData();

        firebaseAuth = FirebaseAuth.getInstance();
        //todo: remove afterwards
        firebaseAuth.signOut();
        progressDialog = new ProgressDialog(this);

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user != null){
            finish();
            startActivity(new Intent(LogInActivity.this,HomeActivity.class));
        }
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValidate(loginUserEmail.getText().toString(),loginUserPassword.getText().toString());

            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogInActivity.this,RegistrationActivity.class));
            }
        });
    }

    @SuppressLint("WrongViewCast")
    void bindingloginData(){
        loginUserEmail = (EditText)findViewById(R.id.loginUserEmail);
        loginUserPassword = (EditText) findViewById(R.id.loginUserPassword);
        signUp = (TextView) findViewById(R.id.loginSignUp);
        signIn = (Button) findViewById(R.id.loginSignIn);
    }

    void checkValidate(String usermail, String password){
        progressDialog.setMessage("Loading...");
        firebaseAuth.signInWithEmailAndPassword(usermail,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    progressDialog.dismiss();
                    checkEmailVerification();
                    //Toast.makeText(LogInActivity.this,"Login successful",Toast.LENGTH_LONG).show();
                    //startActivity(new Intent(LogInActivity.this,HomeActivity.class));
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(LogInActivity.this,"Login Failed",Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public void  checkEmailVerification(){
        FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();
        Boolean emailVerification = firebaseUser.isEmailVerified();

        if(emailVerification){
            finish();
            startActivity(new Intent(LogInActivity.this,HomeActivity.class));
        }
        else{
            Toast.makeText(this,"verify email",Toast.LENGTH_LONG).show();
            firebaseAuth.signOut();
        }

    }


}
