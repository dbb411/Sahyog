package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.adapters.NotificationRecyclerAdapter;
import com.dbb411.myapplication.models.NotificationItem;

public class NotificationActivity extends AppCompatActivity {

    /** Android Views **/
    RelativeLayout rlJumpFeed;
    TextView tvFeed;
    RelativeLayout rlJumpNotification;
    TextView tvNotification;
    RelativeLayout rlJumpDirectory;
    TextView tvDirectory;
    RelativeLayout rlJumpAccount;
    TextView tvAccount;
    RecyclerView rvNotification;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        rlJumpFeed = (RelativeLayout) findViewById(R.id.rl_jump_feed);
        tvFeed = (TextView) findViewById(R.id.tv_feed);
        rlJumpNotification = (RelativeLayout) findViewById(R.id.rl_jump_notification);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        rlJumpDirectory = (RelativeLayout) findViewById(R.id.rl_jump_directory);
        tvDirectory = (TextView) findViewById(R.id.tv_directory);
        rlJumpAccount = (RelativeLayout) findViewById(R.id.rl_jump_account);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        rvNotification = (RecyclerView) findViewById(R.id.rv_notification);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getSupportActionBar().setTitle("Notification");
        bindViews();
        setAllClickListeners();
        loadDataInRecyclerView();
    }

    private void loadDataInRecyclerView() {
        NotificationRecyclerAdapter recyclerAdapter;
        recyclerAdapter = new NotificationRecyclerAdapter(this, NotificationItem.getDummyNotificationItemList());
        rvNotification.setLayoutManager(new LinearLayoutManager(this));
        rvNotification.setAdapter(recyclerAdapter);
    }

    /**
     * This will assign click listeners to all the quick jump buttons.(Relative layouts)
     */
    private void setAllClickListeners() {
        //avoid feed click as we are already on the page
        rlJumpFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this,HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                NotificationActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        //click for notification
        //avoid this as we are already on notification screen

        //click for directory
        rlJumpDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this,DirectoryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                NotificationActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for account
        rlJumpAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationActivity.this,AccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                NotificationActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
    }
}
