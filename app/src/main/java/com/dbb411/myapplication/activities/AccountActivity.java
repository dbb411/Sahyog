package com.dbb411.myapplication.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.UserData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AccountActivity extends AppCompatActivity {

    /** Android Views **/
    RelativeLayout rlJumpFeed;
    TextView tvFeed;
    RelativeLayout rlJumpNotification;
    TextView tvNotification;
    RelativeLayout rlJumpDirectory;
    TextView tvDirectory;
    RelativeLayout rlJumpAccount;
    TextView tvAccount;
    TextView tvUserName,tvUserEmail;
    LinearLayout llMyAccount,llEditProfile,llNotificationSetting,llMyCompline,llLogout,llComplainToMe;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    public String user_name;
    /** Android Views **/

    /**
     * Binds XML views
     * Call this function after setContentView() in onCreate().
     **/
    private void bindViews(){
        rlJumpFeed = (RelativeLayout) findViewById(R.id.rl_jump_feed);
        tvFeed = (TextView) findViewById(R.id.tv_feed);
        rlJumpNotification = (RelativeLayout) findViewById(R.id.rl_jump_notification);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        rlJumpDirectory = (RelativeLayout) findViewById(R.id.rl_jump_directory);
        tvDirectory = (TextView) findViewById(R.id.tv_directory);
        rlJumpAccount = (RelativeLayout) findViewById(R.id.rl_jump_account);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        llLogout = (LinearLayout) findViewById(R.id.ll_logout);
        llMyAccount = (LinearLayout) findViewById(R.id.ll_account);
        llMyCompline = (LinearLayout) findViewById(R.id.ll_my_complain);
        llNotificationSetting = (LinearLayout) findViewById(R.id.ll_notification_setting);
        llEditProfile = (LinearLayout) findViewById(R.id.ll_edit_profile);
        tvUserName = (TextView) findViewById(R.id.tv_userName);
        tvUserEmail = (TextView) findViewById(R.id.tv_userEmail);
        llComplainToMe = (LinearLayout) findViewById(R.id.ll_complain_to_me);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        bindViews();
        getSupportActionBar().setTitle("Account");
        setAllClickListeners();
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        setUserNameAndEmail();
    }

    public void setUserNameAndEmail(){
        DatabaseReference databaseReference = firebaseDatabase.getReference("Users/"+firebaseAuth.getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserData userData = dataSnapshot.getValue(UserData.class);

                user_name = userData.userName;
                tvUserName.setText(userData.getUserName());
                tvUserEmail.setText(userData.getUserEmail());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(AccountActivity.this, "something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * This will assign click listeners to all the quick jump buttons.(Relative layouts)
     */
    private void setAllClickListeners() {
        rlJumpFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this,HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                AccountActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });



        //click for Notification
        rlJumpNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this,NotificationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                AccountActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for AccountActivity
        //avoid this as we are already on AccountActivity screen

        //click for DirectoryActivity
        rlJumpDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this,DirectoryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                AccountActivity.this.startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        //click for logout
        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
                finish();
                Intent intent = new Intent(AccountActivity.this,LogInActivity.class);
                AccountActivity.this.startActivity(intent);
            }
        });

        llComplainToMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this,ComplainToMeActvity.class));
            }
        });

        llMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountActivity.this,MyProfileActivity.class);
                AccountActivity.this.startActivity(intent);
            }
        });

        llMyCompline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this,MyComplainActivity.class));
            }
        });
        llEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AccountActivity.this, "Feature not available", Toast.LENGTH_SHORT).show();
                
            }
        });
        llNotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AccountActivity.this, "Feature not available", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
