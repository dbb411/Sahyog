package com.dbb411.myapplication.models;

public class ComplainData {
    public String complainTitle,complainDescription,complainTo,complainBy,complainerId;

    public ComplainData() {
    }

    public ComplainData (String title, String description, String by, String to,String id){
        this.complainTitle = title;
        this.complainDescription= description;
        this.complainBy = by;
        this.complainerId = id;

        if(to.isEmpty()){
            this.complainTo = "none";
        }
        else
            this.complainTo = to;
    }
}
