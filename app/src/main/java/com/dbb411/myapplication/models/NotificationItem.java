package com.dbb411.myapplication.models;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhruv on 10/25/2017.
 */

public class NotificationItem {
    String title, body, time, imgURL, postedBy, assignedAuthority, assignedDepartment;

    public NotificationItem(String title, String body, String time, String imgURL, String postedBy, String assignedAuthority, String assignedDepartment) {
        this.title = title;
        this.body = body;
        this.time = time;
        this.imgURL = imgURL;
        this.postedBy = postedBy;
        this.assignedAuthority = assignedAuthority;
        this.assignedDepartment = assignedDepartment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getAssignedAuthority() {
        return assignedAuthority;
    }

    public void setAssignedAuthority(String assignedAuthority) {
        this.assignedAuthority = assignedAuthority;
    }

    public String getAssignedDepartment() {
        return assignedDepartment;
    }

    public void setAssignedDepartment(String assignedDepartment) {
        this.assignedDepartment = assignedDepartment;
    }

    public static List<NotificationItem> getDummyNotificationItemList(){
        List<NotificationItem> NotificationItems = new ArrayList<>();
        String dummyImg1 = "www.manninjurylaw.com/wp-content/uploads/2012/08/auto-accident.jpg";
        String dummyImg2 ="http://images.junkmail.co.za/images/thumb/2016/4/13/e26d5897fb424d68284643de47541469115084669c6635158c9a2e979b12f296378818167.jpeg";
        NotificationItems.add(new NotificationItem("Notification1 ","Im body one. please do something","October 23, 2017",dummyImg1,"Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification2 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification3 ","Im body one. please do something","October 23, 2017",dummyImg2,"Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification4 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification5 ","Im body one. please do something","October 23, 2017",dummyImg2,"Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification6 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification7 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU"));
        NotificationItems.add(new NotificationItem("Notification* ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU"));
        return NotificationItems;
    }
}

