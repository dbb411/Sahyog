package com.dbb411.myapplication.models;

public class UserData {
    public String userEmail,userName;

    public  UserData(){}

    public UserData(String userName,String userEmail){
        this.userEmail=userEmail;
        this.userName=userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
