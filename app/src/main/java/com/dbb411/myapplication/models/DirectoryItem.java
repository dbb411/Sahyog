package com.dbb411.myapplication.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhruv on 10/25/2017.
 */

public class DirectoryItem {
    String title, body, time, imgURL, postedBy, assignedAuthority, assignedDepartment;

    public DirectoryItem(String title, String body, String imgURL, String assignedAuthority, String assignedDepartment) {
        this.title = title;
        this.body = body;
        this.imgURL = imgURL;
        this.assignedAuthority = assignedAuthority;
        this.assignedDepartment = assignedDepartment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getAssignedAuthority() {
        return assignedAuthority;
    }

    public void setAssignedAuthority(String assignedAuthority) {
        this.assignedAuthority = assignedAuthority;
    }

    public String getAssignedDepartment() {
        return assignedDepartment;
    }

    public void setAssignedDepartment(String assignedDepartment) {
        this.assignedDepartment = assignedDepartment;
    }

    public static List<DirectoryItem> getDummyDirectoryItemList(){
        List<DirectoryItem> DirectoryItems = new ArrayList<>();
        String dummyImg2 ="@drawable/ic_account_box_black_24dp.xml";
        DirectoryItems.add(new DirectoryItem("7069841197 ","dhruvbbhakta@gmail.com",dummyImg2, "Dhruv Bhakt", "RBI"));
        DirectoryItems.add(new DirectoryItem("7069841197 ","Person2@gmail.com",dummyImg2, "Harsh Patel", "GTU"));
        DirectoryItems.add(new DirectoryItem("8866607722 ","Person3@gmail.com",dummyImg2, "mahipal yogi", "GTU"));
        DirectoryItems.add(new DirectoryItem("7069813909 ","Person4@gmail.com",dummyImg2, "raj yogi", "GTU"));
        DirectoryItems.add(new DirectoryItem("9875438779 ","Person5@gmail.com",dummyImg2, "meet Patel", "GTU"));
        DirectoryItems.add(new DirectoryItem("0874646667 ","Person6@gmail.com",dummyImg2, "Karan Patel", "GTU"));
        DirectoryItems.add(new DirectoryItem("9887776666 ","Person7@gmail.com",dummyImg2, "Sharad Patel", "GTU"));
        DirectoryItems.add(new DirectoryItem("9898765123 ","Person8@gmail.com",dummyImg2, "Amit Patel", "GTU"));
        return DirectoryItems;
    }
}
