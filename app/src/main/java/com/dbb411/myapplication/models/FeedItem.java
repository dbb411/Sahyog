package com.dbb411.myapplication.models;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HBB on 24-10-2017.
 */

public class FeedItem {
    private String title, body, time, imgURL, postedBy, assignedAuthority, assignedDepartment;
    private int upvotes;
    private static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;

    public FeedItem(String title, String body, String postedBy, String assignedAuthority) {
        this.title = title;
        this.body = body;
        this.postedBy = postedBy;
        this.assignedAuthority = assignedAuthority;
    }

    private FeedItem(String title, String body, String time, String imgURL, String postedBy, String assignedAuthority, String assignedDepartment, int upvotes) {
        this.title = title;
        this.body = body;
        this.time = time;
        this.imgURL = imgURL;
        this.postedBy = postedBy;
        this.assignedAuthority = assignedAuthority;
        this.assignedDepartment = assignedDepartment;
        this.upvotes = upvotes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getAssignedAuthority() {
        return assignedAuthority;
    }

    public void setAssignedAuthority(String assignedAuthority) {
        this.assignedAuthority = assignedAuthority;
    }

    public String getAssignedDepartment() {
        return assignedDepartment;
    }

    public void setAssignedDepartment(String assignedDepartment) {
        this.assignedDepartment = assignedDepartment;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public static List<FeedItem> getDummyFeedItemList(){

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        List<FeedItem> feedItems = new ArrayList<>();
        String dummyImg1 = "www.manninjurylaw.com/wp-content/uploads/2012/08/auto-accident.jpg";
        String dummyImg2 ="http://images.junkmail.co.za/images/thumb/2016/4/13/e26d5897fb424d68284643de47541469115084669c6635158c9a2e979b12f296378818167.jpeg";
        feedItems.add(new FeedItem("TItle1 ","Im body one. please do something","October 23, 2017",dummyImg1,"Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle2 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle3 ","Im body one. please do something","October 23, 2017",dummyImg2,"Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle4 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle5 ","Im body one. please do something","October 23, 2017",dummyImg2,"Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle6 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle7 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU",34));
        feedItems.add(new FeedItem("TItle8 ","Im body one. please do something","October 23, 2017","url","Dhruv Bhakta", "Amit Patel", "GTU",34));
        return feedItems;
    }

    public FeedItem() {

    }
}
