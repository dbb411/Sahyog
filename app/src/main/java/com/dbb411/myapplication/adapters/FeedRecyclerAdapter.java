package com.dbb411.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.FeedItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by HBB for DBB on 24-10-2017.
 */

public class FeedRecyclerAdapter extends Adapter<FeedRecyclerAdapter.FeedViewHolder> {
    Context context;
    LayoutInflater layoutInflator;
    List<FeedItem> feedItemList;

    public FeedRecyclerAdapter(Context context, List<FeedItem> feedItemList) {
        this.context = context;

        this.feedItemList = feedItemList;
        layoutInflator = LayoutInflater.from(context);
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = layoutInflator.inflate(R.layout.vh_feed,parent,false);
        FeedViewHolder feedViewHolder = new FeedViewHolder(rootView);
        return feedViewHolder;
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        FeedItem currentFeedItem = feedItemList.get(position);
        holder.attachToFeedItem(currentFeedItem);
    }

    @Override
    public int getItemCount() {
        return feedItemList.size();
    }

     class FeedViewHolder extends RecyclerView.ViewHolder{
         /** Android Views **/
         ImageView imgThumb;
         TextView tvTitle;
         TextView tvBody;
         TextView tvPostedBy;
         TextView tvAssignedTo;
         LinearLayout llUpvoteHolder;
         TextView tvUpvoteCount;
         /** Android Views **/

         /**
          * Binds XML views
          * Call this function after layout is ready.
          **/
         private void bindViews(View rootView){
             imgThumb = (ImageView) rootView.findViewById(R.id.img_thumb);
             tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
             tvBody = (TextView) rootView.findViewById(R.id.tv_body);
             tvPostedBy = (TextView) rootView.findViewById(R.id.tv_posted_by);
             tvAssignedTo = (TextView) rootView.findViewById(R.id.tv_assigned_to);
             llUpvoteHolder = (LinearLayout) rootView.findViewById(R.id.ll_upvote_holder);
             tvUpvoteCount = (TextView) rootView.findViewById(R.id.tv_upvote_count);
         }
        public FeedViewHolder(View itemView) {
            super(itemView);
            bindViews(itemView);
        }

         public void attachToFeedItem(FeedItem currentFeedItem) {
            //title
             tvTitle.setText(currentFeedItem.getTitle());

             //body
             tvBody.setText(currentFeedItem.getBody());

             //posted by
             tvPostedBy.setText("Posted by "+currentFeedItem.getPostedBy());

             //image
             Picasso.with(context).load(currentFeedItem.getImgURL()).error(R.drawable.ic_format_list_bulleted_black_24dp).into(imgThumb);
             //assigned to
             tvAssignedTo.setText("Assignee: "+currentFeedItem.getAssignedAuthority()+" ("+currentFeedItem.getAssignedDepartment()+")");
         }
     }
}
