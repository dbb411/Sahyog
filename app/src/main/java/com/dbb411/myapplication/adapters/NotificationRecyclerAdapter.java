package com.dbb411.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.NotificationItem;
import com.squareup.picasso.Picasso;

import java.util.List;
/**
 * Created by dhruv on 10/25/2017.
 */

public class NotificationRecyclerAdapter extends Adapter<NotificationRecyclerAdapter.NotificationViewHolder> {
    Context context;
    LayoutInflater layoutInflator;
    List<NotificationItem> notificationItemList;

    public NotificationRecyclerAdapter(Context context, List<NotificationItem> notificationItemList) {
        this.context = context;

        this.notificationItemList = notificationItemList;
        layoutInflator = LayoutInflater.from(context);
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = layoutInflator.inflate(R.layout.vh_notification,parent,false);
        NotificationViewHolder notificationViewHolder = new NotificationViewHolder(rootView);
        return notificationViewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        NotificationItem currentNotificationItem = notificationItemList.get(position);
        holder.attachToNotificationItem(currentNotificationItem);
    }

    @Override
    public int getItemCount() {
        return notificationItemList.size();
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder{
        /** Android Views **/
        ImageView imgThumb;
        TextView tvTitle;
        TextView tvBody;
        TextView tvPostedBy;
        TextView tvAssignedTo;
        LinearLayout llUpvoteHolder;
        TextView tvUpvoteCount;
        /** Android Views **/

        /**
        * Binds XML views
        * Call this function after layout is ready.
        **/
        private void bindViews(View rootView){
            imgThumb = (ImageView) rootView.findViewById(R.id.img_thumb);
            tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
            tvBody = (TextView) rootView.findViewById(R.id.tv_body);
            tvPostedBy = (TextView) rootView.findViewById(R.id.tv_posted_by);
            tvAssignedTo = (TextView) rootView.findViewById(R.id.tv_assigned_to);
            llUpvoteHolder = (LinearLayout) rootView.findViewById(R.id.ll_upvote_holder);
            tvUpvoteCount = (TextView) rootView.findViewById(R.id.tv_upvote_count);
        }
        public NotificationViewHolder(View itemView) {
            super(itemView);
            bindViews(itemView);
        }

        public void attachToNotificationItem(NotificationItem currentNotificationItem) {
            //title
            tvTitle.setText(currentNotificationItem.getTitle());

            //body
            tvBody.setText(currentNotificationItem.getBody());

            //posted by
            tvPostedBy.setText("Posted by "+currentNotificationItem.getPostedBy()+" on "+currentNotificationItem.getTime());

            //image
            Picasso.with(context).load(currentNotificationItem.getImgURL()).into(imgThumb);
            //assigned to
            tvAssignedTo.setText("Assignee: "+currentNotificationItem.getAssignedAuthority()+" ("+currentNotificationItem.getAssignedDepartment()+")");
        }
    }
}
