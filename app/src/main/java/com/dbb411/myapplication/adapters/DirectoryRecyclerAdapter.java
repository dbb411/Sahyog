package com.dbb411.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dbb411.myapplication.R;
import com.dbb411.myapplication.models.DirectoryItem;
import com.squareup.picasso.Picasso;

import java.util.List;
/**
 * Created by dhruv on 10/25/2017.
 */

public class DirectoryRecyclerAdapter extends Adapter<DirectoryRecyclerAdapter.DirectoryViewHolder> {
    Context context;
    LayoutInflater layoutInflator;
    List<DirectoryItem> directoryItemList;

    public DirectoryRecyclerAdapter(Context context, List<DirectoryItem> directoryItemList) {
        this.context = context;

        this.directoryItemList = directoryItemList;
        layoutInflator = LayoutInflater.from(context);
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = layoutInflator.inflate(R.layout.vh_directory,parent,false);
        DirectoryViewHolder directoryViewHolder = new DirectoryViewHolder(rootView);
        return directoryViewHolder;
    }

    @Override
    public void onBindViewHolder(DirectoryViewHolder holder, int position) {
        DirectoryItem currentDirectoryItem = directoryItemList.get(position);
        holder.attachToDirectoryItem(currentDirectoryItem);
    }

    @Override
    public int getItemCount() {
        return directoryItemList.size();
    }

    class DirectoryViewHolder extends RecyclerView.ViewHolder{
        /** Android Views **/
        ImageView imgThumb;
        TextView tvTitle;
        TextView tvBody;
        TextView tvPostedBy;
        TextView tvAssignedTo;
        LinearLayout llUpvoteHolder;
        /** Android Views **/

        /**
         * Binds XML views
         * Call this function after layout is ready.
         **/
        private void bindViews(View rootView){
            imgThumb = (ImageView) rootView.findViewById(R.drawable.ic_account_box_black_24dp);
            tvTitle = (TextView) rootView.findViewById(R.id.tv_assigned_to);
            tvBody = (TextView) rootView.findViewById(R.id.tv_body);
            tvPostedBy = (TextView) rootView.findViewById(R.id.tv_posted_by);
            tvAssignedTo = (TextView) rootView.findViewById(R.id.tv_title);
            llUpvoteHolder = (LinearLayout) rootView.findViewById(R.id.ll_upvote_holder);

        }
        public DirectoryViewHolder(View itemView) {
            super(itemView);
            bindViews(itemView);
        }

        public void attachToDirectoryItem(DirectoryItem currentDirectoryItem) {
            //title
            tvTitle.setText(currentDirectoryItem.getTitle());

            //body
            tvBody.setText(currentDirectoryItem.getBody());

            //posted by
            //tvPostedBy.setText("Posted by "+currentDirectoryItem.getPostedBy()+" on "+currentDirectoryItem.getTime());

            //image
            //Picasso.with(context).load(R.drawable.ic_account_box_black_24dp).into(imgThumb);
            //assigned to
            tvAssignedTo.setText(currentDirectoryItem.getAssignedAuthority()+" ("+currentDirectoryItem.getAssignedDepartment()+")");
        }
    }
}
